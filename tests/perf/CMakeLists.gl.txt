
include_directories(
	${GLEXT_INCLUDE_DIR}
	${OPENGL_INCLUDE_PATH}
)

link_libraries (
	piglitutil_${piglit_target_api}
	${OPENGL_gl_LIBRARY}
)

piglit_add_executable (copytex copytex.c common.c)
piglit_add_executable (drawoverhead drawoverhead.c common.c)
piglit_add_executable (draw-prim-rate draw-prim-rate.c common.c)
piglit_add_executable (fbobind fbobind.c common.c)
piglit_add_executable (fill fill.c common.c)
piglit_add_executable (genmipmap genmipmap.c common.c)
piglit_add_executable (pbobench pbobench.c common.c)
piglit_add_executable (pixel-rate pixel-rate.c common.c)
piglit_add_executable (readpixels readpixels.c common.c)
piglit_add_executable (shader-io-rate shader-io-rate.c common.c)
piglit_add_executable (teximage teximage.c common.c)
piglit_add_executable (vbo vbo.c common.c)

# vim: ft=cmake:
