link_libraries (
	piglitutil_${piglit_target_api}
)

piglit_add_executable (apple_sync-client-wait-errors ClientWaitSync-errors.c)
piglit_add_executable (apple_sync-delete DeleteSync.c)
piglit_add_executable (apple_sync-fence-sync-errors FenceSync-errors.c)
piglit_add_executable (apple_sync-get-sync-errors GetSynciv-errors.c)
piglit_add_executable (apple_sync-is-sync IsSync.c)
piglit_add_executable (apple_sync-repeat-wait repeat-wait.c)
piglit_add_executable (apple_sync-sync-initialize sync-initialize.c)
piglit_add_executable (apple_sync-timeout-zero timeout-zero.c)
piglit_add_executable (apple_sync-WaitSync-errors WaitSync-errors.c)
